package com.rodrigo.objects;

public class Ticket {

	private String id, title, priority;
	
	public Ticket(){
		
	}
	
	public String getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getPriority() {
		return priority;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
	
}
