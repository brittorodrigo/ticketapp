package com.rodrigo.controllers;

import java.util.Collection;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.rodrigo.objects.Ticket;
import com.rodrigo.services.TicketService;

@Controller
@RequestMapping(value="/api/v1/tickets")
public class TicketController {

	private static TicketService ticketService = TicketService.getInstance();



	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Ticket>> getTickets(){

		Collection<Ticket> allTickets = ticketService.findAll();
		return new ResponseEntity<Collection<Ticket>>(allTickets, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Ticket> createTicket(@RequestBody Ticket newTicket){
		Ticket ticketCreated = ticketService.create(newTicket);
		return new ResponseEntity<Ticket>(ticketCreated, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}",
			method = RequestMethod.GET)
	public ResponseEntity<Ticket> getTicket(@PathVariable String id){
		Ticket ticket = ticketService.findOne(id);
		if(ticket == null){
			return new ResponseEntity<Ticket>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Ticket>(ticket, HttpStatus.OK);
	}

	@RequestMapping(value="{id}",
			method = RequestMethod.PUT)
	public ResponseEntity<Ticket> updateTicket(@RequestBody Ticket ticket, @PathVariable String id){
		Ticket updatedTicket = ticketService.update(id,ticket);
		if(updatedTicket == null){
			return new ResponseEntity<Ticket>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Ticket>(updatedTicket, HttpStatus.OK);
	}

	@RequestMapping(value="{id}",
			method = RequestMethod.DELETE)
	public ResponseEntity<Ticket> deleteTicket(@PathVariable String id){
		boolean deletedTicket = ticketService.delete(id);
		if(!deletedTicket){
			return new ResponseEntity<Ticket>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Ticket>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(
			method = RequestMethod.DELETE)
	public ResponseEntity<Ticket> deleteTickets(@RequestParam("ids") List<String> ids){
		boolean deletedTicket = ticketService.delete(ids) ;
		if(!deletedTicket){
			return new ResponseEntity<Ticket>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Ticket>(HttpStatus.NO_CONTENT);
	}
}
