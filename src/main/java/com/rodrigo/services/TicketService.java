package com.rodrigo.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rodrigo.objects.Ticket;

public class TicketService {

	private Map<String, Ticket> ticketsMap = new HashMap<String, Ticket>();
	private static TicketService instance;
	private static Long nextId = 1L;
	
	private  TicketService() {
	}
	
	public static TicketService getInstance(){
		return (instance == null?new TicketService(): instance);
	}
	
	private  Ticket save(Ticket ticket){
		ticket.setId(String.valueOf(nextId));
		nextId += 1;
		ticketsMap.put(ticket.getId(), ticket);
		return ticket;

	}
	
	private  Ticket updateTicket(String id, Ticket ticket){
		if(!ticketsMap.containsKey(id)){
			return null;
		}
		ticket.setId(id);
		ticketsMap.put(id, ticket);
		return ticket;
	}
	
	private  boolean deleteTicket(String id){
		Ticket removed = ticketsMap.remove(id);
		if(removed == null){
			return false;
		}
		return true;
	}
	
	public Collection<Ticket> findAll() {
		Collection<Ticket> allTickets = ticketsMap.values();
		return allTickets;
	}

	public Ticket findOne(String id) {
		Ticket ticketFound = ticketsMap.get(id);
		return ticketFound;
	}

	public Ticket create(Ticket newTicket) {
		Ticket ticketCreated = save(newTicket);
		return ticketCreated;
	}

	public Ticket update(String id, Ticket ticket) {
		Ticket updatedTicket = updateTicket(id,ticket);
		return updatedTicket;
	}

	public boolean delete(String id) {
		boolean deletedTicket = deleteTicket(id);
		return deletedTicket;
	}
	
	public boolean delete(List<String> ids) {
		for (String id : ids) {
			if(!delete(id)){
				return false;
			}
		}
		return true;
	}
}
